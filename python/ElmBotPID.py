__author__ = ' Nauss'

import json
from GameInfo import *
import math
from TrackInfo import *
from Regulator import *

class ElmBotPID(object):

    def __init__(self, socket, name, key, track_name):
        self.socket = socket
        self.name = name
        self.key = key
        self.track_name = track_name
        self.gameinfos = GameInfo()
        self.previous_distance = -1.0
        self.previous_piece = -1
        self.speed_file = open("speed.csv", "w")
        self.tick = 0
        self.sent_switch = False
        self.PID = PID(0.5,0.4,0.5)
        self.PID.setPoint(6.42)

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("joinRace", { "botId": { "name": self.name, "key": self.key }, "trackName": "keimola", "carCount": 1 })

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    #Ici c'est le nerf de la guerre

    def on_car_positions(self, data):
        angle = 0
        piece_index = 0
        in_piece_distance = 0.0
        start_lane = 0
        end_lane = 0
        lap = 0
        for d in data:
            if d["id"]["name"] == self.name:
                angle = d["slip_angle"]
                piece_index = d["piecePosition"]["pieceIndex"]
                in_piece_distance = d["piecePosition"]["inPieceDistance"]
                start_lane = d["piecePosition"]["lane"]["startLaneIndex"]
                end_lane = d["piecePosition"]["lane"]["endLaneIndex"]
                lap = d["piecePosition"]["lap"]

        # Switch lane ?
        change_lane(self, piece_index, end_lane)

        if start_lane != end_lane:
            self.sent_switch = False

        #calcul de la vitesse instantanee
        speed = 0.0
        if self.previous_piece >= 0:
            if self.previous_piece == piece_index:
                speed = in_piece_distance - self.previous_distance
            else:
                if self.gameinfos.pieces[self.previous_piece].type == "straight":
                    speed = self.gameinfos.pieces[self.previous_piece].length - self.previous_distance + in_piece_distance
                else:
                    #Et oui c'est pas si simple...
                    lane_length = abs(self.gameinfos.pieces[self.previous_piece].angle*3.141592/180)
                    #Ce if est laid mais jsais pas top comment faire autrement, y'a pas de sign()
                    if self.gameinfos.pieces[self.previous_piece].angle > 0:
                        lane_length *= (self.gameinfos.pieces[self.previous_piece].radius - self.gameinfos.lanes.get(end_lane))
                    else:
                        lane_length *= (self.gameinfos.pieces[self.previous_piece].radius + self.gameinfos.lanes.get(end_lane))

                    speed = lane_length - self.previous_distance + in_piece_distance

        pid = abs(self.PID.update(speed))
        print("pid: " + str(pid) + " speed: " + str(speed))
        if pid > 0 and speed != 0:
            self.throttle(0)
        else:
            self.throttle(1.0)

        self.previous_piece = piece_index
        self.previous_distance = in_piece_distance

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def init_gameinfos(self,data):
        self.gameinfos.init(data)

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.init_gameinfos
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if "gameTick" in msg:
                self.tick = msg['gameTick']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()
