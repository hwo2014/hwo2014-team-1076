__author__ = 'Patator'

import json
from GameInfo import *
import math
import random
from TrackInfo import *
from CarInfo import *

class EstimationBot(object):

    def __init__(self, socket, name, key, track_name):
        self.socket = socket
        self.name = name
        self.key = key
        self.track_name = track_name
        self.gameinfos = GameInfo()
        self.previous_distance = -1.0
        self.previous_piece = -1
        self.previous_speed = 0
        self.angles = [0, 0]
        self.speed_file = open("speed.csv", "w")
        self.speed_file.write("slip_angle;speed;curvature;centrifugal;constant;\n")
        self.tick = 0
        self.sent_switch = False
        self.turbo_available = False

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        if self.track_name != "":
            return self.msg("joinRace", { "botId": { "name": self.name, "key": self.key }, "trackName": self.track_name, "carCount": 1 })
        else:
            return self.msg("join", { "name": self.name, "key": self.key })

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    #Ici c'est le nerf de la guerre

    def on_car_positions(self, data):
        # if(self.tick == 2):
        #     self.msg("switchLane", "Right")
        #     return
        angle = 0.0
        piece_index = 0
        in_piece_distance = 0.0
        start_lane = 0
        end_lane = 0
        lap = 0
        for d in data:
            if d["id"]["name"] == self.name:
                angle = d["slip_angle"]
                piece_index = d["piecePosition"]["pieceIndex"]
                in_piece_distance = d["piecePosition"]["inPieceDistance"]
                start_lane = d["piecePosition"]["lane"]["startLaneIndex"]
                end_lane = d["piecePosition"]["lane"]["endLaneIndex"]
                lap = d["piecePosition"]["lap"]

        #calcul de la vitesse instantanee
        speed = get_speed(self, end_lane, in_piece_distance, piece_index)

        #rayon de courbure
        curvature = get_curvature(self.gameinfos.pieces[piece_index], self.gameinfos.lanes, end_lane)

        #Switch ?
        change_lane(self, piece_index, end_lane)

        target_speed = 5.9

        target_a = 0.01
        # throttle = target_speed/10.0
        if speed < target_speed:
            throttle = 1.0
        else:
            #acceleration constante
            throttle = speed/10

        self.throttle(throttle)
        centrifugal = 0
        if curvature != 0:
            centrifugal = speed*speed/curvature
        # if(centrifugal > 0):
        #     print (str(speed) + " " + str(slip_angle/centrifugal))

        a = -0.00125 * self.previous_speed + 1.9
        b = -0.9
        constant = angle - a * self.angles[1] - b * self.angles[0]
        self.speed_file.write(str(angle)+";"+str(speed)+";"+str(curvature)+";"+str(centrifugal)+";"+str(constant)+";\n")

        if start_lane != end_lane:
            self.sent_switch = False

        # slip_angle(t-2) = self.slip_angle[0]
        # slip_angle(t-1) = self.slip_angle[1]
        self.angles[0] = self.angles[1]
        self.angles[1] = angle
        self.previous_speed = speed
        self.previous_piece = piece_index
        self.previous_distance = in_piece_distance

    def ticks_to_target_speed(self,current_speed, target_speed):
        if target_speed > current_speed:
            ticks = math.log((target_speed-10.0)/(current_speed-10.0),0.98)
        elif target_speed < current_speed:
            ticks = math.log(target_speed/current_speed,0.98)
        else:
            return 0

        return math.ceil(ticks)

    def length_to_target_speed(self,current_speed, target_speed):
        n = self.ticks_to_target_speed(current_speed, target_speed)
        if target_speed > current_speed:
            return (current_speed - 10)*(1-0.98**n)/(0.02)+ n*10.0
        elif target_speed < current_speed:
            return current_speed*(1-0.98**n)/(0.02)
        else:
            return 0

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()
        self.throttle_value = 0.59

    def on_game_end(self, data):
        print("Race ended")

        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def init_gameinfos(self,data):
        self.gameinfos.init(data)

    def on_turbo_avaible(self, data):
        self.turbo_available = True
        self.turbo_duration_tick = data["turboDurationTicks"]

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.init_gameinfos,
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if "gameTick" in msg:
                self.tick = msg['gameTick']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()
