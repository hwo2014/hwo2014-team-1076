__author__ = 'Bruno'

from GameInfo import *
import math


class CarInfo:
    def __init__(self, car, key=""):
        self.name = "ElmBot"
        self.key = key
        if car == 0:
            return
        print("Adding car name")
        self.name = car["id"]["name"]
        print("name = " + self.name)
        print("Adding car length")
        self.length = car["dimensions"]["length"]
        print("length = " + str(self.length))
        print("Adding car width")
        self.width = car["dimensions"]["width"]
        print("width = " + str(self.width))
        print("Adding car guideFlagPosition")
        self.guide_flag_position = car["dimensions"]["guideFlagPosition"]
        print("guideFlagPosition = " + str(self.guide_flag_position))
        self.slip_angle = 0.0
        self.piece_index = -1
        self.previous_piece = -1
        self.in_piece_distance = -1
        self.previous_in_piece_distance = -1
        self.start_lane = 0
        self.end_lane = 0
        self.lap = 0

    def from_data(self, data):
        for d in data:
            if d["id"]["name"] == self.name:
                self.slip_angle = d["angle"]
                self.piece_index = d["piecePosition"]["pieceIndex"]
                self.in_piece_distance = d["piecePosition"]["inPieceDistance"]
                self.start_lane = d["piecePosition"]["lane"]["startLaneIndex"]
                self.end_lane = d["piecePosition"]["lane"]["endLaneIndex"]
                self.lap = d["piecePosition"]["lap"]
                break

    def get_speed(self, bot):
        speed = 0.0
        if self.previous_piece >= 0:
            if self.previous_piece == self.piece_index:
                speed = self.in_piece_distance - self.previous_in_piece_distance
            else:
                if bot.gameInfo.pieces[self.previous_piece].type == "straight":
                    speed = (bot.gameInfo.pieces[self.previous_piece].length - self.previous_in_piece_distance) + self.in_piece_distance
                else:
                    #Et oui c'est pas si simple...
                    lane_length = abs(bot.gameInfo.pieces[self.previous_piece].angle * math.pi / 180.0)

                    if bot.gameInfo.pieces[self.previous_piece].angle > 0:
                        lane_length *= (bot.gameInfo.pieces[self.previous_piece].radius - bot.gameInfo.lanes.get(self.end_lane))
                    else:
                        lane_length *= (bot.gameInfo.pieces[self.previous_piece].radius + bot.gameInfo.lanes.get(self.end_lane))

                    speed = (lane_length - self.previous_in_piece_distance) + self.in_piece_distance

        return speed