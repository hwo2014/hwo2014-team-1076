__author__ = 'Patator'

from CarInfo import *


class TrackPiece:
    def __init__(self):
        self.type = "None"
        self.length = 0.0
        self.switch = False
        self.radius = 0.0
        self.angle = 0.0
        self.turbotable = False


class GameInfo:
    def __init__(self):
        self.type = "qualifying"   # or "race"
        self.pieces = []
        self.lanes = {}
        self.cars = []
        self.laps = 0
        self.duration_ms = 0
        self.max_lap_time_ms = 0
        self.quick_race = False
        self.nb_pieces = 0

    def init(self, bot, data):
        race = data["race"]
        track = race["track"]
        pieces = track["pieces"]
        lanes = track["lanes"]
        race_cars = race["cars"]

        for l in lanes:
            self.lanes[l["index"]] = l["distanceFromCenter"]

        print(self.lanes)

        cur = 0
        for p in pieces:
            piece = TrackPiece()
            # print("Piece " + str(cur))
            if "length" in p:
                piece.length = p["length"]
                piece.type = "straight"
                # print("Type: " + str(piece.type) + " length: " + str(piece.length))
            else:
                piece.radius = p["radius"]
                piece.angle = p["angle"]
                piece.type = "bend"
                # print("Type: " + str(piece.type) + " angle: " + str(piece.angle) + " radius: " + str(piece.radius))

            if "switch" in p:
                piece.switch = p["switch"]
                # print("Switch")
            else:
                piece.switch = False
                # print("No switch")
            self.pieces.append(piece)

            # print("")
            cur += 1

        self.nb_pieces = len(self.pieces)

        session = race["raceSession"]
        if "laps" in session:
            self.type = "race"
            self.laps = session["laps"]
            self.max_lap_time_ms = session["maxLapTimeMs"]
            self.quick_race = session["quickRace"]
        if "duration_ms" in session:
            self.type = "qualifying"
            self.duration_ms = session["duration_ms"]

        # Cars
        for car in race_cars:
            print("Creating car")
            carInfo = CarInfo(car)
            if carInfo.name == bot.carInfo.name:
                bot.carInfo = carInfo
            else:
                self.cars.append(carInfo)

    def get_length(self, piece_index, lane):
        piece = self.pieces[piece_index % len(self.pieces)]
        length = 0
        if piece.type == "straight":
            length = piece.length
        else:
            if piece.angle > 0:
                length = abs(piece.angle * 3.141592 / 180.0) * (piece.radius - self.lanes[lane])
            else:
                length = abs(piece.angle * 3.141592 / 180.0) * (piece.radius + self.lanes[lane])
        return length


    def get_distance(self, car1, car2):
        pieces = self.pieces
        nb_pieces = len(pieces)
        if car1.piece_index == car2.piece_index:
            return car2.in_piece_distance - car1.in_piece_distance
        distance = self.get_length(car1.piece_index, car1.end_lane) - car1.in_piece_distance + car2.in_piece_distance
        index = (car1.piece_index + 1) % nb_pieces
        if index == car2.piece_index:
            return distance
        while True:
            distance += self.get_length(index, car1.end_lane)
            index = (index + 1) % nb_pieces
            if index == car2.piece_index:
                break

        return distance

    def get_closest_car(self, car):
        pieces = self.pieces
        nb_pieces = len(pieces)
        closest = 0
        min_dist = 100000
        for other in self.cars:
            if car.name == other.name:
                continue
            distance = self.get_distance(car, other)
            if distance < min_dist:
                min_dist = distance
                closest = other

        return closest, min_dist
