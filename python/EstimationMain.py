import socket
import ElmBotv1
import ElmBotPID
import EstimationBot
import EstimationBot2
import sys

host = sys.argv[1]
port = sys.argv[2]
name = sys.argv[3]
key = sys.argv[4]

print("Connecting with parameters:")
print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print("Connection to socket")
s.connect((host, int(port)))
print("Connected")

track_name = ""
bot = EstimationBot2.EstimationBot2(s, name, key, track_name)
bot.run()
