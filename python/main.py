import socket
import ElmBotv1
import ElmBotPID
import EstimationBot
import sys

host = sys.argv[1]
port = sys.argv[2]
name = sys.argv[3]
key = sys.argv[4]

print("Connecting with parameters:")
print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print("Connection to socket")
s.connect((host, int(port)))
print("Connected")

# Pass the name of the track as the 4th parameter
# Nothing to join the default race (CI)
track_name = ""
bot = ElmBotv1.ElmBotv1(s, name, key, track_name)
bot.run()
