__author__ = 'Patator'

import json
from GameInfo import *
import math
from TrackInfo import *
from CarInfo import *

class ElmBotv1(object):

    def __init__(self, socket, name, key, track_name):
        self.socket = socket
        self.gameInfo = GameInfo()
        self.trackInfo = TrackInfo(track_name)
        self.carInfo = CarInfo(0, key)

        # self.speed_file = open("speed.csv", "w")
        self.tick = 0
        self.sent_switch = False
        self.turbo_available = False
        self.turbo_duration_tick = 0
        self.turbo_factor = 1.0
        self.in_turbo = False
        self.theta1 = 0.0
        self.theta2 = 0.0
        self.vprec = 0.0
        self.turbo_piece = 0
        # Estimation
        self.v_speeds = []
        self.v_throttles = []
        self.bend_passed = 0
        self.theta_speeds = []
        self.theta_angles = []
        self.theta_curvatures = []
        self.first_estimation_piece = -1

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        if self.trackInfo.track_name != "":
            return self.msg("joinRace", { "botId": { "name": self.carInfo.name, "key": self.carInfo.key }, "trackName": self.trackInfo.track_name, "carCount": 1 })
        else:
            return self.msg("join", { "name": self.carInfo.name, "key": self.carInfo.key })

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.send(json.dumps({"msgType": "ping"}))

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")

    def on_game_start(self, data):
        print("Race started")
        self.ping()
        # Inits
        self.tick = 0
        self.sent_switch = False
        self.turbo_available = False
        self.turbo_duration_tick = 0
        self.turbo_factor = 1.0
        self.in_turbo = False
        self.theta1 = 0.0
        self.vprec = 0.0
        self.turbo_piece = 0
        # Estimation
        self.v_speeds = []
        self.v_throttles = []
        self.bend_passed = 0
        self.theta_speeds = []
        self.theta_angles = []
        self.theta_curvatures = []
        # Other cars
        self.others_states = []

    def beta_k_estimation(self, curvature):
        if self.trackInfo.theta_in_bend == 0 and abs(self.carInfo.slip_angle) > 0 and curvature != 0:
            # Start logging for beta and k
            self.trackInfo.theta_in_bend = curvature
        if self.trackInfo.theta_in_bend != 0 and curvature != self.trackInfo.theta_in_bend:
            # Stop  logging
            self.trackInfo.theta_in_bend = 0

            # Estimation
            self.trackInfo.determine_angle(self.theta_angles, self.theta_speeds, self.theta_curvatures, False)

            # Reset parameters
            self.theta_speeds = []
            self.theta_angles = []
            self.theta_curvatures = []

    def on_car_positions(self, data):
        self.carInfo.from_data(data)
        for other in self.gameInfo.cars:
            other.from_data(data)

        # Useful variables
        current_piece = self.gameInfo.pieces[self.carInfo.piece_index]
        current_radius = current_piece.radius
        angle_sign = 1
        if current_piece.type == "bend" and current_piece.angle < 0:
            angle_sign = -1

        slip_angle = self.carInfo.slip_angle #* angle_sign
        curvature = self.trackInfo.get_curvature(self, current_piece)

        x = self.trackInfo.theta["x"]
        y = self.trackInfo.theta["y"]
        alpha = self.trackInfo.theta["alpha"]
        beta, k = self.trackInfo.get_beta_k(curvature)

        #calcul de la vitesse instantanee
        speed = self.carInfo.get_speed(self)

        # Changement de ligne ?
        change_lane_direction = self.trackInfo.change_lane(self)

        if self.carInfo.start_lane != self.carInfo.end_lane:
            self.sent_switch = False

        #calcul de la longueur jusqu'au prochain virage
        length_to_next_bend, next_bend_curvature = self.trackInfo.get_length_to_next_bend(self)
        # length_to_next_bend = self.gameInfo.pieces[self.carInfo.piece_index].length - self.carInfo.in_piece_distance
        # has_switch = False
        # bend_direction = ""
        # n = 1
        # next_bend = TrackPiece()
        # while (True):
        #     if self.gameInfo.pieces[(self.carInfo.piece_index+n)%self.gameInfo.nb_pieces].switch:
        #         has_switch = True
        #     if(self.gameInfo.pieces[(self.carInfo.piece_index+n)%self.gameInfo.nb_pieces].type == "bend") and current_radius != self.gameInfo.pieces[(self.carInfo.piece_index+n)%self.gameInfo.nb_pieces].type:
        #         next_bend = self.gameInfo.pieces[(self.carInfo.piece_index+n)%self.gameInfo.nb_pieces]
        #         break
        #     length_to_next_bend += self.gameInfo.pieces[(self.carInfo.piece_index+n)%self.gameInfo.nb_pieces].length
        #     n += 1


        current_throttle = 1.0
        target_speed_in_next_bend = 10.0
        if self.trackInfo.OK():
            #vitesse atteint apres le turbo
            # if self.turbo_available:
            #     speed_after_turbo = (self.trackInfo.speed["a"]**self.turbo_duration_tick)*(speed - self.turbo_factor*10.0)+10.0*self.turbo_factor
            #
            #     length_to_brake = self.length_to_target_speed(speed_after_turbo,target_speed_in_next_bend,0.0)
            #     if length_to_next_bend > self.length_to_target_speed(speed_after_turbo,target_speed_in_next_bend,0.0) and current_piece.type == "straight":
            #         self.turbo_available = False
            #         print("turbo")
            #         self.msg("turbo", "pouet")
            #         self.in_turbo = True
            #         return

            # Turbo ?
            if self.turbo_piece == self.carInfo.piece_index and self.carInfo.in_piece_distance > (self.gameInfo.get_length(self.carInfo.piece_index, self.carInfo.end_lane) / 2):
                self.turbo_piece = -1
                self.msg("turbo", "T'es serieux la ?")
                self.in_turbo = True
                print("Turbo on piece " + str(self.carInfo.piece_index))

            # ksi1 = 1.78
            # ksi2 = -0.78
            # ksi3 = 0.01
            # PID
            # Kp = ksi3
            # Kd = (ksi1 + ksi3 - x - ksi2 + y) / 2
            # Ki = (ksi1 + ksi3 - x + ksi2 - y) / 2
            Kp = 0.009
            Kd = -0.1
            Ki = 0
            target_angle = angle_sign * (59.2 if self.trackInfo.OK() else 45.0)
            if current_piece.type == "bend":
                # Soft bend
                # if abs(beta) < 0.001:
                #     if speed < target_speed_in_next_bend:
                #         current_throttle = 1.0
                #     else:
                #         current_throttle = self.trackInfo.speed_to_throttle(speed, target_speed_in_next_bend)
                # else :
                commande_virage = Kp * (target_angle - slip_angle) + Kd * (slip_angle - self.theta1) + Ki * (slip_angle + self.theta1)

                delta = (alpha * self.carInfo.slip_angle) ** 2 - 4 * (beta * angle_sign) * (k * angle_sign - commande_virage) / curvature
                target_speed1 = (- alpha * self.carInfo.slip_angle + sqrt(delta)) / (2 * (beta * angle_sign) / curvature)
                target_speed2 = (- alpha * self.carInfo.slip_angle - sqrt(delta)) / (2 * (beta * angle_sign) / curvature)
                target_speed = min(10.0, max(0.0, max(target_speed1, target_speed2)))
                print("target_speed:" + str(target_speed))
                if speed < target_speed:
                    current_throttle = self.trackInfo.speed_to_throttle(speed, target_speed)
                else:
                    length_to_brake = self.length_to_target_speed(speed, target_speed, 0.0)
                    if length_to_next_bend <= length_to_brake:
                        current_throttle = 0.0
                    else:
                        current_throttle = self.trackInfo.speed_to_throttle(speed, target_speed)

                # Force throttle to 1 when going out a curve
                next_piece = self.gameInfo.pieces[(self.carInfo.piece_index + 1) % len(self.gameInfo.pieces)]
                if next_piece.type == "straight":
                    piece_length = 0
                    if self.gameInfo.pieces[self.carInfo.piece_index].angle > 0:
                        piece_length *= (self.gameInfo.pieces[self.carInfo.piece_index].radius - self.gameInfo.lanes.get(self.carInfo.end_lane))
                    else:
                        piece_length *= (self.gameInfo.pieces[self.carInfo.piece_index].radius + self.gameInfo.lanes.get(self.carInfo.end_lane))
                    if self.carInfo.in_piece_distance > piece_length / 2.0:
                        current_throttle = 1.0
            else:
                commande = Kp * (target_angle - slip_angle) + Kd * (slip_angle - self.theta1) + Ki * (slip_angle + self.theta1)
                # F(v) = (beta / C) * (Vt-1 * Vt-1) + alpha * Theta_t-1 * Vt-1 + k = commande
                beta_next, k_next = self.trackInfo.get_beta_k(next_bend_curvature)
                delta = (alpha * angle_sign * target_angle) ** 2 - 4 * (beta_next * angle_sign) * (k_next * angle_sign) / next_bend_curvature
                target_speed_in_next_bend1 = (- alpha * angle_sign * target_angle + sqrt(delta)) / (2 * (beta_next * angle_sign) / next_bend_curvature)
                target_speed_in_next_bend2 = (- alpha * angle_sign * target_angle - sqrt(delta)) / (2 * (beta_next * angle_sign) / next_bend_curvature)
                target_speed_in_next_bend = min(10.0, max(0.0, max(target_speed_in_next_bend1, target_speed_in_next_bend2)))
                if speed < target_speed_in_next_bend:
                    current_throttle = 1.0
                else:
                    length_to_brake = self.length_to_target_speed(speed, target_speed_in_next_bend, 0.0)
                    if length_to_next_bend <= length_to_brake:
                        current_throttle = 0.0
                    else:
                        current_throttle = 1.0

            if self.in_turbo and current_throttle != 1.0:
                current_throttle /= self.turbo_factor

            # Continuous estimation
            self.beta_k_estimation(curvature)
            # Do log
            if self.trackInfo.theta_straight_with_slip or self.trackInfo.theta_in_bend:
                self.theta_speeds.append(speed)
                self.theta_angles.append(self.carInfo.slip_angle)
                self.theta_curvatures.append(angle_sign * curvature)
        # Estimation
        else:
            # Speed to drift
            current_throttle = 1.0
            if speed > 7.0:
                current_throttle = 0
            # Next is a bend
            if length_to_next_bend < current_piece.length:
                # brake
                current_throttle = 0
            elif current_piece.type == "bend":
                drift = slip_angle # - x * self.theta1 - y * self.theta2
                if abs(drift) > 20.0:
                    current_throttle = 0
            if speed > 4.0 and next_bend_curvature < 50:
                current_throttle = 0

            # Estimation
            # V=f(throttle)
            if not self.trackInfo.speed_OK:
                if speed != 0:
                    self.v_speeds.append(speed)
                    self.v_throttles.append(current_throttle)
                    # Enough args > fucking get the parameters
                    nb_speeds = len(self.v_speeds)
                    if nb_speeds > 2:
                        self.trackInfo.determine_V(self.v_speeds, self.v_throttles)

            # Angle
            if not self.trackInfo.OK():
                # First part (x, y, alpha)
                if not self.trackInfo.theta_xyalpha_OK:
                    if not self.trackInfo.theta_straight_with_slip:
                        if slip_angle != 0 and curvature == 0:
                            # Start logging
                            self.trackInfo.theta_straight_with_slip = True
                    elif slip_angle == 0 or curvature != 0:
                        # Stop  logging
                        self.trackInfo.theta_straight_with_slip = False

                        # Estimation
                        self.trackInfo.determine_angle(self.theta_angles, self.theta_speeds, self.theta_curvatures, True)

                        self.first_estimation_piece = (self.carInfo.piece_index - 1) % self.gameInfo.nb_pieces

                        # Reset parameters
                        self.theta_speeds = []
                        self.theta_angles = []
                        self.theta_curvatures = []
                else:
                    # Second part (beta, k)
                    self.beta_k_estimation(curvature)

                    if self.first_estimation_piece == self.carInfo.piece_index:
                        self.trackInfo.lap_finished()

                # Do log
                if self.trackInfo.theta_straight_with_slip or self.trackInfo.theta_in_bend:
                    self.theta_speeds.append(speed)
                    self.theta_angles.append(self.carInfo.slip_angle)
                    self.theta_curvatures.append(angle_sign * curvature)
                    # self.theta_curvatures.append(angle_sign * curvature)


        # Check others
        closest, distance = self.gameInfo.get_closest_car(self.carInfo)
        if not self.sent_switch and closest != 0 and distance < 2 * closest.length and self.carInfo.end_lane == closest.end_lane and speed > closest.get_speed(self):
            # Force switch
            if self.carInfo.end_lane > 0:
                self.msg("switchLane", "Left")
                self.sent_switch = True
            elif self.carInfo.end_lane < len(self.gameInfo.lanes):
                self.msg("switchLane", "Right")
                self.sent_switch = True

        # print(str(self.carInfo.slip_angle) + "  " + str(speed) + "  " + str(curvature))
        self.carInfo.previous_piece = self.carInfo.piece_index
        self.carInfo.previous_in_piece_distance = self.carInfo.in_piece_distance
        for other in self.gameInfo.cars:
            other.previous_piece = other.piece_index
            other.previous_in_piece_distance = other.in_piece_distance

        if self.gameInfo.laps != 0 and self.carInfo.piece_index == len(self.gameInfo.pieces) - 1 and self.carInfo.lap == self.gameInfo.laps - 1:
            # Last piece
            current_throttle = 1.0

        print("speed: " + str(speed) + "    target_speed_in_next_bend: " + str(target_speed_in_next_bend) + "   angle: " + str(self.carInfo.slip_angle) + "    pieces_index: " + str(self.carInfo.piece_index) + "   throttle: " + str(current_throttle))
        # Should be the last thing to do
        self.throttle(current_throttle)

        self.theta2 = self.theta1
        self.theta1 = slip_angle
        self.vprec = speed
        # self.speed_file.write(str(speed) + ";" + str(self.carInfo.slip_angle) + ";" + str(curvature) + "\n")

    def ticks_to_target_speed(self,current_speed, target_speed, throttle):
        if current_speed - 10.0 * throttle == 0:
            return 0
        to_log = (target_speed-10.0*throttle)/(current_speed-10.0*throttle)
        if (to_log > 0.0):
            ticks = math.log(to_log, self.trackInfo.speed["a"])
        else:
            # /!!!!!!!! ATTENTION VOIR SI CA C'EST BON !!!!!!!\
            ticks = 0

        return math.ceil(ticks)

    def length_to_target_speed(self,current_speed, target_speed, throttle):
        n = self.ticks_to_target_speed(current_speed, target_speed, throttle)
        length = (current_speed - 10*throttle)*(1-self.trackInfo.speed["a"]**n)/(self.trackInfo.speed["b"]/10.0)+n*10.0*throttle
        # Make sure we do not brake in the last straight pieces
        if self.gameInfo.laps != 0 and self.carInfo.lap == self.gameInfo.laps - 1 and self.gameInfo.pieces[self.carInfo.piece_index].type == "straight":
            rest = self.gameInfo.get_length(self.carInfo.piece_index, self.carInfo.end_lane) - self.carInfo.in_piece_distance
            for i in range(self.carInfo.piece_index + 1, self.gameInfo.nb_pieces):
                if self.gameInfo.pieces[i].type == "straight":
                    rest += self.gameInfo.get_length(i, self.carInfo.end_lane)
                else:
                    rest = 100000000
                    break
            if rest < length:
                length = 0
        return length

    def on_crash(self, data):
        print("Someone crashed")

    def on_game_end(self, data):
        print("Race ended")

    def on_error(self, data):
        print("Error: {0}".format(data))

    def init_gameinfos(self, data):
        self.gameInfo.init(self, data)

    def on_turbo_available(self, data):
        self.turbo_available = True
        self.turbo_duration_tick = data["turboDurationTicks"]
        self.turbo_factor = data["turboFactor"]
        self.trackInfo.get_turbo_piece(self)
        print("Turbo available: " + str(self.turbo_duration_tick) + " ms  factor: " + str(self.turbo_factor))

    def on_lap_finished(self,data):
        print("lap time: " + str(data["lapTime"]["millis"]))

    def on_turbo_end(self,data):
        self.in_turbo = False

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.init_gameinfos,
            'turboAvailable': self.on_turbo_available,
            'lapFinished': self.on_lap_finished,
            'turboEnd': self.on_turbo_end
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if "gameTick" in msg:
                self.tick = msg['gameTick']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                # self.ping()
            line = socket_file.readline()
