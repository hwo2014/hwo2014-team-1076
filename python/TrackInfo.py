__author__ = 'Bruno'

import cPickle as pickle
from GameInfo import *
from numpy import *

# A gauche : negatif
# A droite : positif

class TrackInfo:
    def __init__(self, track_name):
        self.track_name = track_name
        # Vt = a * Vt-1 + b * throttle
        self.speed_OK = False
        self.speed = {"a": 0.98, "b": 0.2}
        # Theta_t = [x y alpha beta k][Theta_t-1        ]
        #                             [Theta_t-2        ]
        #                             [Vt-1 * Theta_t-1 ]
        #                             [Vt * Vt / C           ]
        #                             [1                ]
        self.theta_xyalpha_OK = False
        self.theta_betak_OK = False
        self.theta = {"x": 0, "y": 0, "alpha": 0.0, "beta_k": [{"curvature": 0.0, "r2": 0, "beta": 0.0, "k": 0.0}]}
        # That should be true when saving the speed/slip/curvature to determine x, y and alpha
        self.theta_straight_with_slip = False
        # That should be true when saving the speed/slip/curvature to determine beta and k
        self.theta_in_bend = False

    def OK(self):
        return self.speed_OK and self.theta_xyalpha_OK and self.theta_betak_OK

    def pieces_to_next_switch(self, bot):
        pieces = []

        nb_pieces = bot.gameInfo.nb_pieces
        next_piece = bot.carInfo.piece_index

        while True:
            piece = bot.gameInfo.pieces[next_piece]
            pieces.append(piece)
            if (piece.switch == True):
                break
            next_piece += 1
            if next_piece == nb_pieces:
                next_piece = 0

        return pieces

    def get_length_to_next_bend(self, bot):
        pieces = bot.gameInfo.pieces
        nb_pieces = bot.gameInfo.nb_pieces
        piece = pieces[bot.carInfo.piece_index]
        current_radius = 0 if pieces[bot.carInfo.piece_index].type == "straight" else piece.radius

        found = current_radius == 0
        length = self.get_lengths(bot, [pieces[bot.carInfo.piece_index]])[bot.carInfo.end_lane] - bot.carInfo.in_piece_distance
        n = 1
        while True:
            piece = pieces[(bot.carInfo.piece_index + n) % nb_pieces]
            if piece.type == "straight":
                found = True
            elif piece.radius != current_radius:
                return length, self.get_curvature(bot, piece)
            elif found:
                return length, self.get_curvature(bot, piece)
            length += piece.length
            n += 1

        return length, self.get_curvature(bot, piece)

    def change_lane(self, bot):
        if bot.sent_switch:
            return 0

        # Find all the pieces between the next 2 switches
        pieces = []

        nb_pieces = bot.gameInfo.nb_pieces
        next_piece = bot.carInfo.piece_index

        # Find next switch
        while True:
            piece = bot.gameInfo.pieces[next_piece]
            if (piece.switch == True):
                # Push the first piece (with the switch)
                pieces.append(piece)
                next_piece += 1
                break
            next_piece += 1
            if next_piece == nb_pieces:
                next_piece = 0

        # We get the pieces until the next switch
        while True:
            piece = bot.gameInfo.pieces[next_piece]
            if (piece.switch == True):
                break
            pieces.append(piece)
            next_piece += 1
            if next_piece == nb_pieces:
                next_piece = 0

        # Compute the length of the lanes between the switches
        lengths = self.get_lengths(bot, pieces)

        if len(lengths) == 0:
            return 0

        # Check if all the paths have the same length
        sameLength = True
        firstLength = lengths[0]
        for length in lengths:
            if length != firstLength:
                sameLength = False
                break

        if sameLength:
            return 0

        # Index of the shortest lane
        min_index = lengths.index(min(lengths))

        # Switch or not switch
        switch_to = ""
        end_line_distance = bot.gameInfo.lanes[bot.carInfo.end_lane]
        min_lane_distance = bot.gameInfo.lanes[min_index]
        if end_line_distance != min_lane_distance:
            switch_to = "Right"
            if end_line_distance > min_lane_distance:
                switch_to = "Left"

        if switch_to != "":
            bot.msg("switchLane", switch_to)
            bot.sent_switch = True

        if switch_to == "Right":
            return -1
        else:
            return -2

    def get_lengths(self, bot, pieces):
        lengths = [0] * len(bot.gameInfo.lanes)
        first_piece_length_no_switch = 0
        for l in bot.gameInfo.lanes:
            for i, p in enumerate(pieces):
                length = 0
                if p.type == "straight":
                    length += p.length
                else:
                    if p.angle > 0:
                        length = abs(p.angle * 3.141592 / 180.0) * (p.radius - bot.gameInfo.lanes[l])
                    else:
                        length = abs(p.angle * 3.141592 / 180.0) * (p.radius + bot.gameInfo.lanes[l])

                if i == 0 and l == bot.carInfo.start_lane:
                    first_piece_length_no_switch = length

                lengths[l] += length

        # Handle first piece
        first_piece = pieces[0]
        lanes = bot.gameInfo.lanes

        first_piece_length_switch1 = 0
        first_piece_length_switch2 = 0
        if first_piece.type == "bend":
            if p.angle > 0:
                r1 = first_piece.radius - lanes[bot.carInfo.start_lane]
            else:
                r1 = first_piece.radius + lanes[bot.carInfo.start_lane]

            if bot.carInfo.start_lane > 0:
                if p.angle > 0:
                    r2 = first_piece.radius - lanes[bot.carInfo.start_lane - 1]
                else:
                    r2 = first_piece.radius + lanes[bot.carInfo.start_lane - 1]
                first_piece_length_switch2 = sqrt(r1 ** 2 + r2 ** 2 - 2 * r1 * r2 * cos(first_piece.angle * 3.141592 / 180.0))
                lengths[bot.carInfo.start_lane - 1] += first_piece_length_switch2 - first_piece_length_no_switch
            if bot.carInfo.start_lane < (len(lanes) - 1):
                if p.angle > 0:
                    r2 = first_piece.radius - lanes[bot.carInfo.start_lane + 1]
                else:
                    r2 = first_piece.radius + lanes[bot.carInfo.start_lane + 1]
                first_piece_length_switch1 = sqrt(r1 ** 2 + r2 ** 2 - 2 * r1 * r2 * cos(first_piece.angle * 3.141592 / 180.0))
                lengths[bot.carInfo.start_lane + 1] += first_piece_length_switch1 - first_piece_length_no_switch
        else:
            if bot.carInfo.start_lane > 0:
                first_piece_length_switch2 = sqrt(first_piece_length_no_switch ** 2 + (lanes[bot.carInfo.start_lane] - lanes[bot.carInfo.start_lane - 1]) ** 2)
                lengths[bot.carInfo.start_lane - 1] += first_piece_length_switch2 - first_piece_length_no_switch
            if bot.carInfo.start_lane < (len(lanes) - 1):
                first_piece_length_switch1 = sqrt(first_piece_length_no_switch ** 2 + (lanes[bot.carInfo.start_lane] - lanes[bot.carInfo.start_lane + 1]) ** 2)
                lengths[bot.carInfo.start_lane + 1] += first_piece_length_switch1 - first_piece_length_no_switch

        return lengths

    # Will return the piece with the longest following straight line
    # Will also return the lap number where the turbo should be started
    def get_turbo_piece(self, bot):

        pieces = bot.gameInfo.pieces
        nb_pieces = bot.gameInfo.nb_pieces

        p = bot.carInfo.piece_index
        l = bot.carInfo.lap

        max_length = 0
        current_straight = []
        straight_length = 0
        longest_straight = []

        while True:
            if pieces[p].type == "straight":
                current_straight.append(p)
                straight_length += pieces[p].length
            else:
                if straight_length > max_length:
                    longest_straight = current_straight
                    max_length = straight_length
                current_straight = []
                straight_length = 0

            p += 1
            if p == nb_pieces:
                p = 0
                l += 1
                if bot.gameInfo.laps != 0:
                    if l == bot.gameInfo.laps:
                        break
                elif l - self.carInfo.lap == 2:
                    break

        if straight_length > max_length:
            longest_straight = current_straight

        turbo_piece = -1
        if len(longest_straight) != 0:
            turbo_piece = longest_straight[0] - 1
            if turbo_piece < 0:
                turbo_piece = nb_pieces - 1

        if bot.carInfo.lap == bot.gameInfo.laps - 1:
            # dernier et tour et a deux pieces de la fin
            # envoie la sauce
            if bot.carInfo.piece_index == nb_pieces - 2:
                turbo_piece = bot.carInfo.piece_index


        bot.turbo_piece = turbo_piece

    def get_curvature(self, bot, piece):
        curvature = 0.0
        lanes = bot.gameInfo.lanes
        if piece.type == "bend":
            if piece.angle > 0:
                curvature = piece.radius - lanes.get(bot.carInfo.end_lane)
            else:
                curvature = piece.radius + lanes.get(bot.carInfo.end_lane)

        return curvature

    def speed_to_throttle(self, current_speed, target_speed):
        throttle = (target_speed - self.speed["a"] * current_speed) / self.speed["b"]
        if throttle < 0:
            throttle = 0
        if throttle > 1.0:
            throttle = 1.0
        return throttle

    # Compute the distance to the car
    # Exact distance between
    #   - the front of the car
    # def distance_to_closest_car(self, bot):
    #
    # def distance_to_car(self, car1, car1):
    #     for p in range(car1.piece_index, bot.gameInfo.pieces

    # Determine V=f(throttle)
    def determine_V(self, speeds, throttles):
        nb_speeds = len(speeds)
        X = array([speeds[0:nb_speeds-1], throttles[0:nb_speeds-1]]).T
        y = array(speeds[1:nb_speeds])

        a, b = linalg.lstsq(X, y)[0]

        self.speed["a"] = a
        self.speed["b"] = b
        self.speed_OK = True

        print("################## Estimation ##################")
        print("a: " + str(a) + " b: " + str(b))
        print("################################################")

    # Determine Angle=f(v)
    def determine_angle(self, angles, speeds, curvatures, xyalpha):
        # Theta_t = [x y alpha beta k][Theta_t-1        ]
        #                             [Theta_t-2        ]
        #                             [Vt-1 * Theta_t-1 ]
        #                             [Vt * Vt / C           ]
        #                             [1                ]

        # [2] = t
        # [1] = t - 1
        # [0] = t - 2

        nb_angles = len(angles)
        if nb_angles < 5 or nb_angles != len(speeds) or nb_angles != len(curvatures):
            return

        if xyalpha:
            # Vt * Theta_t
            v_theta = [0] * nb_angles
            for i in range(nb_angles):
                v_theta[i] = speeds[i] * angles[i]

            y = array(angles[2:nb_angles])
            X = array([angles[1:nb_angles-1], angles[0:nb_angles-2], v_theta[1:nb_angles-1]]).T

            x, y, alpha = linalg.lstsq(X, y)[0]
            self.theta["x"] = x
            self.theta["y"] = y
            self.theta["alpha"] = alpha

            self.theta_xyalpha_OK = True
            print("################## Estimation ##################")
            print("x: " + str(self.theta["x"]) + " y: " + str(self.theta["y"]) + " alpha: " + str(self.theta["alpha"]))
            print("################################################")
        else:
            # Theta_t - x * Theta_t-1 - y * Theta_t-2 - alpha * Vt-1 * Theta_t-1
            full_angles = []
            for i in range(0, nb_angles - 2):
                full_angles.append(angles[i + 2] - self.theta["x"] * angles[i + 1] - self.theta["y"] * angles[i] - self.theta["alpha"] * speeds[i + 1] * angles[i + 1])

            # Vt * Vt / C
            accel = [0] * nb_angles
            for i in range(nb_angles):
                accel[i] = speeds[i] * speeds[i] / curvatures[i]

            # k
            ones_zeros = [0] * nb_angles
            for i in range(nb_angles):
                if curvatures[i] > 0:
                    ones_zeros[i] = 1
                elif curvatures[i] < 0:
                    ones_zeros[i] = -1

            y = array(full_angles)
            X = array([accel[1:nb_angles-1], ones_zeros[1:nb_angles-1]]).T

            solution = linalg.lstsq(X, y)
            beta, k = solution[0]
            residuals = solution[1]
            r2 = (1 - residuals / (y.size * y.var()))[0]

            curvature = abs(curvatures[0])
            found = False
            for beta_k in self.theta["beta_k"]:
                if curvature == beta_k["curvature"]:
                    found = True
                    if r2 > beta_k["r2"]:
                        beta_k["beta"] = beta
                        beta_k["k"] = k
                        # Continuous estimation
                        if self.theta_betak_OK:
                            print("Changing curvature: " + str(beta_k["curvature"]) + "  r2: " + str(beta_k["r2"]) + " beta: " + str(beta_k["beta"]) + "   k: " + str(beta_k["k"]))
            if not found:
                self.theta["beta_k"].append({"curvature": curvature, "r2": r2, "beta": beta, "k": k})
                # Continuous estimation
                if self.theta_betak_OK:
                    print("Adding curvature: " + str(beta_k["curvature"]) + "  r2: " + str(beta_k["r2"]) + " beta: " + str(beta_k["beta"]) + "   k: " + str(beta_k["k"]))



    def lap_finished(self):
        self.theta_betak_OK = True
        print("################## Estimation ##################")
        for beta_k in self.theta["beta_k"]:
            print("curvature: " + str(beta_k["curvature"]) + "  r2: " + str(beta_k["r2"]) + " beta: " + str(beta_k["beta"]) + "   k: " + str(beta_k["k"]))
        print("################################################")


    def get_beta_k(self, curvature):
        min_diff = 100000
        ret = 0
        for beta_k in self.theta["beta_k"]:
            diff = abs(beta_k["curvature"] - abs(curvature))
            if diff < min_diff:
                min_diff = diff
                ret = beta_k

        return ret["beta"], ret["k"]