__author__ = 'Patator'

import socket
import ElmBotv1
import ElmBotPID
import EstimationBot
import sys
import csv
import tickSequenceBot
import random
import threading

host = sys.argv[1]
port = sys.argv[2]
name = sys.argv[3]
key = sys.argv[4]

print("Connecting with parameters:")
print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))

print("Connected")

#bot = ElmBotPID.ElmBotPID(s, name, key)
#bot = ElmBotv1.ElmBotv1(s, name, key)
tick_file = open("ticksequenceBase.txt","r")
reader = csv.reader(tick_file, delimiter=",")
tick_sequence = []
for row in reader:
    tick_sequence.append(float(row[0]))

print (tick_sequence)

#Algorithme genetique pour optimiser la sequence de ticks
nb_bots = 100
bots = []
threads = []
best_tick_sequence = []

for i in range(nb_bots):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, int(port)))
    bot = tickSequenceBot.TickSequenceBot(s, name + str(i), key)
    bots.append(bot)
    bot.tick_sequence = tick_sequence[:]

for b in bots:
    for i, t in enumerate(b.tick_sequence):
        if(t>=0.0):
            b.tick_sequence[i] = t + random.gauss(0,0.01)
            b.tick_sequence[i] = 1.0 if b.tick_sequence[i] > 1.0 else b.tick_sequence[i]
            b.tick_sequence[i] = 0.0 if b.tick_sequence[i] < 0.0 else b.tick_sequence[i]

for iteration in range(0,1000):
    threads = []
    for b in bots:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        b.socket = s
        threads.append(threading.Thread(None, b.run))
        threads[-1].start()

    for thread in threads:
        thread.join(10*60)

    print("all thread joined\n")

    #all thread joined
    sorted(bots, key = lambda bot: -bot.best_lap_time)

    best_tick_sequence = bots[0].tick_sequence[:]
    print(bots[0].best_lap_time)

    #evolution
    for b in bots:
        for i, t in enumerate(b.tick_sequence):
            if(t>=0.0):
                b.tick_sequence[i] = t + random.gauss(0,0.01)
                b.tick_sequence[i] = 1.0 if b.tick_sequence[i] > 1.0 else b.tick_sequence[i]

    #croisements
    for i in range(0,nb_bots/2):
        split_index = random.randint(1,len(tick_sequence)-2)
        #trouve deux bot parmis les meilleurs
        index1 = random.randint(0,nb_bots/2)
        index2 = random.randint(0,nb_bots/2)
        bots[nb_bots/2+i - 1].tick_sequence[0:split_index] = bots[index1].tick_sequence[0:split_index]
        bots[nb_bots/2+i - 1].tick_sequence[split_index:] = bots[index2].tick_sequence[split_index:]

best_sequence_file = open("best_sequence.txt", "w")

for t in best_tick_sequence:
    best_sequence_file.write(str(t)+"\n")




