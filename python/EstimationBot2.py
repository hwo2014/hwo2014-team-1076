__author__ = 'Patator'

import json
from GameInfo import *
import random
from TrackInfo import *
from CarInfo import *
from TrackInfo import *

class EstimationBot2(object):

    def __init__(self, socket, name, key, track_name):
        self.socket = socket
        self.name = name
        self.key = key
        self.track_name = track_name
        self.gameinfos = GameInfo()
        self.trackinfo = TrackInfo()
        self.previous_distance = -1.0
        self.previous_piece = -1
        self.speeds = []

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        if self.track_name != "":
            return self.msg("joinRace", {"botId": {"name": self.name, "key": self.key}, "trackName": self.track_name, "carCount": 1})
        else:
            return self.msg("join", {"name": self.name, "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        angle = 0.0
        piece_index = 0
        in_piece_distance = 0.0
        start_lane = 0
        end_lane = 0
        lap = 0
        for d in data:
            if d["id"]["name"] == self.name:
                angle = d["slip_angle"]
                piece_index = d["piecePosition"]["pieceIndex"]
                in_piece_distance = d["piecePosition"]["inPieceDistance"]
                start_lane = d["piecePosition"]["lane"]["startLaneIndex"]
                end_lane = d["piecePosition"]["lane"]["endLaneIndex"]
                lap = d["piecePosition"]["lap"]

        throttle = 0.4
        self.throttle(throttle)

        speed = get_speed(self, end_lane, in_piece_distance, piece_index)

        if speed != 0:
            self.speeds.append(speed)

        # Enough args > fucking get the parameters
        nbSpeeds = len(self.speeds)
        if nbSpeeds > 100:
            self.trackinfo.determine_V(self.speeds, throttle)
            exit()

        self.previous_piece = piece_index
        self.previous_distance = in_piece_distance

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")

        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def init_gameinfos(self,data):
        self.gameinfos.init(data)

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.init_gameinfos
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if "gameTick" in msg:
                self.tick = msg['gameTick']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()
