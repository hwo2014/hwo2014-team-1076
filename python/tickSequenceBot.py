__author__ = 'Patator'

__author__ = 'Patator'

import json
from GameInfo import *
import math
from TrackInfo import *

class TickSequenceBot(object):

    def __init__(self, socket, name, key, track_name):
        self.socket = socket
        self.name = name
        self.key = key
        self.track_name = track_name
        self.gameinfos = GameInfo()
        self.tick_sequence = []
        self.best_lap_time = 30000
        self.tick = 0

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        if self.track_name != "":
            return self.msg("joinRace", { "botId": { "name": self.name, "key": self.key }, "trackName": self.track_name, "carCount": 1 })
        else:
            return self.msg("join", { "name": self.name, "key": self.key })

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        #print("Joined")
        self.ping()

    def on_game_start(self, data):
        #print("Race started")
        self.ping()

    #Ici c'est le nerf de la guerre

    def on_car_positions(self, data):

        if(self.tick >= (len(self.tick_sequence) - 1)):
            self.throttle(0.6)
            return

        if(self.tick_sequence[self.tick] >= 0):
            self.throttle(self.tick_sequence[self.tick])
        else:
            if self.tick_sequence[self.tick] == -1:
                self.msg("switchLane", "Right")
            else:
                self.msg("switchLane", "Left")

    def on_crash(self, data):
        #print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        #print("Race ended")
        self.ping()

    def on_error(self, data):
        #print("Error: {0}".format(data))
        self.ping()

    def init_gameinfos(self,data):
        self.gameinfos.init(data)

    def on_lap_finished(self,data):
        #print(data["lapTime"]["millis"])
        if data["lapTime"]["millis"] < self.best_lap_time:
            self.best_lap_time = data["lapTime"]["millis"]

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.init_gameinfos,
            'lapFinished': self.on_lap_finished
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if "gameTick" in msg:
                self.tick = msg['gameTick']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                #print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()

